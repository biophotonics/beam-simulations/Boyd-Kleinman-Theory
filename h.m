function result = h(sigma,beta,kappa,xi,mu)

% we have used that mu*alpha*l = mu*2*kappa*l/b = 2*mu*kappa*xi

h_int=@(tau,tau_dash)exp(-kappa*(tau+tau_dash)+1i*sigma*(tau-tau_dash)-beta^2*(tau-tau_dash).^2)./((1+1i*tau)*(1-1i*tau_dash));
result = abs(1/(4*xi)*exp(2*mu*kappa*xi)*dblquad(h_int,-xi*(1-mu),xi*(1+mu),-xi*(1-mu),xi*(1+mu),1e-6,'quad'));


end