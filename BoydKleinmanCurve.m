clear all



% For non-critical phase matching, beta = 0
% For no absorption, kappa = 0
% For the focus in the middle of the crystal, mu = 0

% Generally, beta = B/sqrt(xi)

B = 0;
kappa = 0;
xi = logspace(-2,3,30);
beta = 0;
mu = 0;

for j = length(xi):-1:1
[sigma_m(j) , minus_h_m] = fminbnd(@(sigma) -h(sigma,beta,kappa,xi(j),mu),atan(xi(j))/xi(j),atan(xi(j)/2)/(xi(j)/2),optimset('TolX',1e-4));

sigma_m
h_m(j) = -minus_h_m;
end

figure(1);
semilogx(xi,sigma_m)
figure(2);
loglog(xi,h_m)
